package com.agiliz.roonga.testcases;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;


public class HomePage extends com.agiliz.roonga.base.BaseTest {
	public static Logger log =LogManager.getLogger(com.agiliz.roonga.base.BaseTest.class.getName());
		
	
	
	@Test(priority=1)
	public void aboutLinkDisplayTest(){

		
hp.aboutLinkCheck();
	
if(true){
	log.info("about link is displayed in the header");	
	}
	}
	@Test(priority=2)
	public void aboutText(){
		
		Assert.assertEquals(hp.aboutLink.getText(), "About");
	}
	
	@Test(priority=3)
	public void whoWeServeText(){
		Assert.assertEquals(hp.whoWeServe.getText(), "Who We Serve");
	}
}
