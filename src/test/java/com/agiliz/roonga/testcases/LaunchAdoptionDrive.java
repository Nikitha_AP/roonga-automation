package com.agiliz.roonga.testcases;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.agiliz.roonga.base.BaseTest;
import com.agiliz.roonga.util.Property;

public class LaunchAdoptionDrive extends BaseTest {
	public static Logger log = LogManager.getLogger(BaseTest.class.getName());

	@Test
	public void nonProfitLoginTest() {
		try {
			lp.login(Property.getProperty("Non-ProfitEmail"), Property.getProperty("Non-ProfitPwd"));
			log.info("Logged in Non-Profit");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.info("Error in login");

		}

	}

	@Test(dependsOnMethods = "nonProfitLoginTest")
	public void launchNewDriveTest() {
		libs.WaitForElementPresentIn(adoptionDrive.launchNewDriveBtn);

		
		try {
			libs.WaitForElementToBeVisible(adoptionDrive.launchNewDriveBtn);
			adoptionDrive.launchNewDrive();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Error occured while selecting the type of drive" + e.getMessage());
			Assert.fail("Error occured when selecting");
		}
	}

	@Test(dependsOnMethods = "launchNewDriveTest")
	public void driveDetailsWizard() throws Exception {

		log.info("Entering drive details in wizard 1");
		try {
			adoptionDrive.driveProgramDetails();
			log.info("Drive details successfully added in wizard 1");
		} catch (Exception e) {
			// TODO Auto-generated catch block

			log.error("Exception in driveProgramDetails:" + e.getMessage());
			Assert.fail("Error creating drive details in wizard 1");
		}

	}

	@Test(dependsOnMethods = "driveDetailsWizard")
	public void profileSubmittersWizard() throws Exception {
		// libs.WaitForElementPresentIn(adoptionDrive.addSocialWorkerDropDown1);
		//libs.waitForPageToLoad();

		try {

			adoptionDrive.profileSubmittersWizard();
			log.info("successfully added social worker");
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Error while creating social worker / profile submitters");
			log.error("Exception in profileSubmittersWizard:" + e.getMessage());
			Assert.fail("Error in adding the profile submitters in the add profile submitters wizard");
		}

	}
	
	@Test(dependsOnMethods="profileSubmittersWizard")
	public void donorGuidelinesWizard(){
		adoptionDrive.setDonorGuidelinesWizard();
	}
	
	@Test(dependsOnMethods="donorGuidelinesWizard")
	public void previewAdoptionDriveWizard(){
		log.info("Verifying the drive name in the preview wizard");
		Assert.assertTrue(adoptionDrive.previewDriveWizard(), "Drive name is displayed in the preview");
		log.info("Drive name is displayed");
		log.info("clicking on post drive button");
		adoptionDrive.postDriveBtnClick();
		log.info("Drive is posted succesfully");
		
	}
	
	@Test(dependsOnMethods="previewAdoptionDriveWizard")
	public void driveActiveStatusTest(){	
	Assert.assertTrue(adoptionDrive.vDrivePostedStatus(), "Drive is in active state");	
	log.info("Drive is in active state");
	}
}
