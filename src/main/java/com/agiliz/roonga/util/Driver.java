package com.agiliz.roonga.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Driver {
	public static WebDriver driver;
	
	
	public static WebDriver getDriver() {	
	if (Property.getProperty("browser").equals("chrome")) {
		System.setProperty("webdriver.chrome.driver", "driver//chromedriver.exe"); 
		driver = new ChromeDriver();
		driver.get(Property.getProperty("URL"));
		
	}
	
	else if (Property.getProperty("browser").equals("firefox")) {
		//FileInputStream fis1 = new FileInputStream("Jar//geckodriver.exe"); 
		//WebDriver driver=(WebDriver) new FileInputStream("Jar/geckodriver.exe");
		
		System.setProperty("webdriver.gecko.driver", "driver//geckodriver.exe");
		driver = new FirefoxDriver();
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(Property.getProperty("URL"));
	}
	
	else if (Property.getProperty("browser").equals("IE")) {
		
		
		
	}
	return driver;

	
	}
}
