package com.agiliz.roonga.util;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CommonLibrary {
	
	
	
	/* WAIT STATEMENTS */
	public void waitForPageToLoad() {
		Driver.driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}
	
	//used only for carosuel image
	public WebElement WaitForElementPresentIn(WebElement wb) {
		WebDriverWait wait = new WebDriverWait(Driver.driver, 50);
		wait.until(ExpectedConditions.visibilityOf(wb));
	return wb;
	}
	
	public WebElement WaitForElementToBeVisible(WebElement wb) {
		WebDriverWait wait = new WebDriverWait(Driver.driver, 30);
		wait.until(ExpectedConditions.visibilityOf(wb));
	return wb;
	}

	public void WaitForElementPresent(WebElement wb) {
		WebDriverWait wait = new WebDriverWait(Driver.driver, 30);
		wait.until(ExpectedConditions.visibilityOf(wb));
	} 

	public void waitForXpathPresent(String wbXpath) {

		WebDriverWait wait = new WebDriverWait(Driver.driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(wbXpath)));

	}

	public void waitForXpathVisible(String wbXpath) {

		WebDriverWait wait = new WebDriverWait(Driver.driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(wbXpath)));

	}

	public void waitForIdVisible(String wbID) {

		WebDriverWait wait = new WebDriverWait(Driver.driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(wbID)));

	}

	public void waitForLinkTextPresent(String linkName) {

		WebDriverWait wait = new WebDriverWait(Driver.driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(linkName)));
	}
	
	public void waitForElementToBeClickable(WebElement elem) {

		WebDriverWait wait = new WebDriverWait(Driver.driver, 50);
		System.out.println("waiting");
		wait.until(ExpectedConditions.elementToBeClickable(elem));
	}
	

	/* END OF WAIT STATEMENTS */

	public void select(WebElement selWb, String value) {
		Select sel = new Select(selWb);
		sel.selectByVisibleText(value);
	}

	public void select(WebElement selWb, int index) {
		Select sel = new Select(selWb);
		sel.selectByIndex(index);
	}

	public void aceptAlert() {
		Alert alt = Driver.driver.switchTo().alert();
		alt.accept();
	}

	public void calcelAlert() {
		Alert alt = Driver.driver.switchTo().alert();
		alt.dismiss();
	}
	
	public String getText() {
		Alert alt = Driver.driver.switchTo().alert();
		return alt.getText();
	}

	public boolean isElementPresent(By by) {
		boolean flag = false;
		try {
			Driver.driver.findElement(by);
			flag = true;
			return flag;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	//Scroll Down
	public void scrollDown() {

		JavascriptExecutor jse = (JavascriptExecutor)Driver.driver;
		jse.executeScript("window.scrollBy(0,1000)", "");

	}

	public void scrollMid() {

		JavascriptExecutor jsem = (JavascriptExecutor)Driver.driver;
		jsem.executeScript("window.scrollBy(0,580)", "");

	}
}
