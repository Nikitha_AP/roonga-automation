package com.agiliz.roonga.util;

import java.io.FileInputStream;
import java.util.Properties;

public class Property {

    private static Properties Props = new Properties(); 
    
    static {
      try {
          FileInputStream in = new FileInputStream("Properties//testdata.properties");
          Props.load(in);
//          in.close();
      } catch (Exception e) {
          e.printStackTrace();
      }
    }
    
    public static String getProperty(String key) {
	      return Props.getProperty(key);
	    }
    
    
}
