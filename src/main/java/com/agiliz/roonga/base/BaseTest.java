package com.agiliz.roonga.base;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import com.agiliz.roonga.pages.AdoptionDriveLaunchPage;
import com.agiliz.roonga.pages.HomePage;
import com.agiliz.roonga.pages.LoginPage;
import com.agiliz.roonga.util.AutoITFirefox;
import com.agiliz.roonga.util.CommonLibrary;
import com.agiliz.roonga.util.Driver;
import com.agiliz.roonga.util.TestUtil;
import com.agiliz.roonga.util.WebEventListener;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;





public class BaseTest {
public static WebDriver driver;
	public static Logger log;
	public static Properties prop;
	public static TestUtil testUtil;
	public static LoginPage lp;
	public static HomePage hp;
	public static ExtentTest extentTest;
	public static ExtentReports extent;
	public static AdoptionDriveLaunchPage adoptionDrive;
	public static CommonLibrary libs;
	public  static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static long PAGE_LOAD_TIMEOUT = 20;
	public static AutoITFirefox autoIt;
	
	
	@BeforeClass
	public static void initializeDriver() throws Exception {
		
		
		
	
		driver = Driver.getDriver();
		
		
		
		Driver.driver.manage().deleteAllCookies();
		
		Driver.driver.manage().window().maximize();
		Driver.driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		Driver.driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		Driver.driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		
		
	e_driver = new EventFiringWebDriver(Driver.driver);
	// Now create object of EventListerHandler to register it with EventFiringWebDriver
	eventListener = new WebEventListener();
	e_driver.register(eventListener);
	Driver.driver = e_driver;
	
	
	libs = new CommonLibrary();
	testUtil = new TestUtil(Driver.driver);
	
	
	
	
	
	
	
	 
	 
	 
	 
	
	}
	
	@BeforeMethod
	public static void beforeMethodTest(){
		lp = new LoginPage(Driver.driver);
		hp = new HomePage(Driver.driver);
		adoptionDrive = new AdoptionDriveLaunchPage(Driver.driver);
		
	}


@AfterClass
public void tearDown()
{
	
	driver.quit();
	
	
	//driver.close();
}
	
	
}
