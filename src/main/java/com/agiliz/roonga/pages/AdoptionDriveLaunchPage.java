package com.agiliz.roonga.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.agiliz.roonga.util.AutoITFirefox;
import com.agiliz.roonga.util.CommonLibrary;
import com.agiliz.roonga.util.Driver;
import com.agiliz.roonga.util.Property;
import com.agiliz.roonga.util.TestUtil;

public class AdoptionDriveLaunchPage {

	public AdoptionDriveLaunchPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		PageFactory.initElements(Driver.driver,this);
	}

AutoITFirefox autoIT = new AutoITFirefox();
CommonLibrary comLibs = new CommonLibrary();
TestUtil testUtil = new TestUtil();

	@FindBy(id="launchNewDriveBtn")
	public WebElement launchNewDriveBtn;
	
	@FindBy(xpath="//div[@class='modal-adoption flex-display column-flex']//div[@class='image-container flex-display']")
	public WebElement adoptionDriveBtn;
	
	@FindBy(id="createBtn")
	public WebElement createBtn;
	
	
	@FindBy(id="driveName")
	public WebElement driveNameTextField;
	
	@FindBy(id="friendlyUrl")
	public WebElement driveURL;
	
	@FindBy(xpath="//*[@id='socialDate']/div/div/div/button")
	public WebElement calenderBtn;
	
	@FindBy(id="driveType")
	public WebElement typeOfDrive;
	
	@FindBy(xpath="//*[@id='c1']/following::label[1]")
	public WebElement profilesAllowedFamily;
	
	@FindBy(xpath="//*[@id='c2']/following::label[1]")
	public WebElement profilesAllowedIndividual;
	
	
	
	@FindBy(xpath="//*[@id='c+0']/following::label[1]")
	public WebElement individualType;
	
	@FindBy(xpath="//*[@id='individuals']/div")
	public WebElement individualsDropDown;
	
	@FindBy(id="photos")
	public WebElement mediaBtn;
	
	@FindBy(xpath="//*[@id='logoupload']/..")
	public WebElement photosBtn;
	
	@FindBy(id="idType")
	public WebElement profileIDType;
	
	
	
	@FindBy(xpath="//*[@class=' individuals dropdown']/button")
	public WebElement addSocialWorkerDropDown1;
	

	@FindBy(xpath="//*[@class='individuals dropdown']/button")
	public WebElement addSocialWorkerDropDown2;

	@FindBy(xpath="//input[@id='c+3']/following::label[1]")
	public WebElement selectSocialWorker;
	
//	@FindBy(xpath="//div[@class='individuals dropdown']/button")
//	public WebElement addSocialWorkerDropDown2;
	
	@FindBy(xpath="//*[@id='nextStepBtn']")
	public WebElement inviteProfileSubmittersNextStepBtn;
	
	@FindBy(xpath="//textarea[@formcontrolname='address']")
	public WebElement addressTextField;
	
	@FindBy(id="contactPerson")
	public WebElement contactPersonTextField;
	
	@FindBy(id="phone")
	public WebElement phoneNumberTextField;
	
	@FindBy(id="email")
	public WebElement emailTextField;
	
	@FindBy(id="previewBtn")
	public WebElement drivePreviewBtn;
	
	@FindBy(xpath="//h4[@class='title-size title-font']")
	public WebElement driveName;
	
	@FindBy(id="postDrive")
	public WebElement postDriveBtn;

	
	
	@FindBy(xpath="//*[@id='image']/following::div[text()='Drive Posted']")
	public WebElement drivePostedPopUp;
	
	@FindBy(xpath="//button[text()='OK']")
	public WebElement okBtn;
	
	
	@FindBy(xpath="//*[text()=' Adoption Drive ']")
	public WebElement adoptionDriveTitle;
	
	@FindBy(xpath="//*[@id='socialDate']//button[@aria-label='Open Calendar']")
	public WebElement submitProfileDateWidget;
	
	@FindBy(xpath="//*[@id='socialDate']//button[@aria-label='Next Month']")
	public WebElement nextMonthBtn;
	
	@FindBy(xpath="//*[@id='donorsDate']//button[1]")
	public WebElement adoptProfileDateWidget;
	
	@FindBy(xpath="//*[@id='donorsDate']//button[@aria-label='Next Month']")
	public WebElement adoptProfileNextMonthBtn;
	
	@FindBy(xpath="//*[@id='recievingDate']//button[1]")
	public WebElement dropOffGiftDateWidget;
	
	@FindBy(xpath="//*[@id='recievingDate']//button[@aria-label='Next Month']")
	public WebElement dropOffGiftsNextMonthBtn;
	
	@FindBy(id="saveBtn")
	public WebElement saveImageBtn;
	
	@FindBy(id="cancelBtn")
	public WebElement cancelImageBtn;
	
	@FindBy(xpath="//*[@class='note-editable panel-body']")
	public WebElement descriptionTextField;
	
	
	@FindBy(xpath="//*[@id='socialDate']//table[@class='caltable']//tbody/tr/td/div/span[text()='15']")
	public WebElement submitProfileDate;
	
	@FindBy(xpath="//*[@id='donorsDate']//table[@class='caltable']//tbody/tr/td/div/span[text()='16']")
	public WebElement adoptProfileDate;
	
	
	@FindBy(xpath="//*[@id='recievingDate']//table[@class='caltable']//tbody/tr/td/div/span[text()='18']")
	public WebElement receivingGiftDate;
	


	@FindBy(id="saveExitBtn")
	public WebElement driveDetailsSaveBtn;
	
	@FindBy(id="searchDrive")
	public WebElement searchDriveTextField;
	
	@FindBy(id="nextStepBtn")
	public WebElement nextBtn;
	
	@FindBy(xpath="//*[text()='Invite Profile Submitters']/../div[1]")
	public WebElement inviteProfilesPage;
	
	
	@FindBy(xpath="//tbody")
	public WebElement socialWorkerDisplay;
	
	@FindBy(xpath="//textarea[@class='form-control ng-pristine ng-invalid ng-touched']")
	public WebElement giftDropOffAddress;
	
	@FindBy(xpath="//*[@id='stDate']//button[@aria-label='Open Calendar']")
	public WebElement giftDropOffStartDateWidget;
	
	@FindBy(xpath="//*[@id='stDate']//button[@aria-label='Next Month']")
	public WebElement giftDropOffNextMonthBtn;
	
	@FindBy(xpath="//*[@id='stDate']//table[@class='caltable']//tbody/tr/td/div/span[text()='17']")
	public WebElement giftDropOffStartDate;
	
	
	@FindBy(xpath="//*[@class='resp-active flex-display']//*[text()='Active']")
	public WebElement driveActiveStatusIcon;
	
	@FindBy(xpath="//*[@class='modal-block']/div/div/button[text()='OK']")
	public WebElement alertOkBtn;
	
	public void launchNewDrive() throws Exception{

		comLibs.WaitForElementPresentIn(launchNewDriveBtn);
		Thread.sleep(10000);
		launchNewDriveBtn.click();
		Thread.sleep(10000);
		adoptionDriveBtn.click();

		createBtn.click();

	}
	
	public boolean adoptionDriveLandingPage() {
		boolean adoptionDriveTitleDisplay = adoptionDriveTitle.isDisplayed();
		Assert.assertEquals(adoptionDriveTitleDisplay, true);
		return adoptionDriveTitleDisplay;
	}
	
	public void driveProgramDetails() throws Exception {
		driveNameTextField.sendKeys(Property.getProperty("driveName"));
		Thread.sleep(5000);
				descriptionTextField.sendKeys(Property.getProperty("description"));
		driveURL.sendKeys(Property.getProperty("driveURL"));
		comLibs.scrollMid();
		submitProfileDateWidget.click();
		nextMonthBtn.click();
		nextMonthBtn.click();
		submitProfileDate.click();
		
		adoptProfileDateWidget.click();
		adoptProfileNextMonthBtn.click();
		adoptProfileNextMonthBtn.click();
		adoptProfileDate.click();
		
		dropOffGiftDateWidget.click();
		dropOffGiftsNextMonthBtn.click();
		dropOffGiftsNextMonthBtn.click();
		receivingGiftDate.click();
		comLibs.scrollDown();
		comLibs.select(typeOfDrive, Property.getProperty("driveType1"));
		profilesAllowedIndividual.click();
		
		individualsDropDown.click();
		individualType.click();
		
		individualsDropDown.click();
		
		
		mediaBtn.click();
		photosBtn.click();
		
		Thread.sleep(10000);
			testUtil.imageUpload();
			Thread.sleep(10000);
			saveImageBtn.click();
		
		comLibs.scrollDown();
		
		Thread.sleep(5000);
		profileIDType.click();
		
		comLibs.select(profileIDType, 1);
		//driveDetailsSaveBtn.click();
		nextBtn.click();
		
		
	}
	
	public boolean vSubmitProfilePageNavigation() {
		boolean inviteProfilesWizard = inviteProfilesPage.isEnabled();
		Assert.assertEquals(inviteProfilesWizard, true);
		return inviteProfilesWizard;
		
	}
	
	public void profileSubmittersWizard() {
    try {
		Thread.sleep(6000);
		comLibs.scrollDown();
		addSocialWorkerDropDown1.click();
		comLibs.WaitForElementToBeVisible(selectSocialWorker);
		selectSocialWorker.click();
		
		addSocialWorkerDropDown2.click();
	boolean vSocialWorkerDisplay = socialWorkerDisplay.isDisplayed();
		Assert.assertEquals(vSocialWorkerDisplay,true );
		nextBtn.click();
		
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		
	}
		
		
	}
	public void setDonorGuidelinesWizard(){
		comLibs.scrollMid();
		addressTextField.sendKeys(Property.getProperty("Address"));
		giftDropOffStartDateWidget.click();
		giftDropOffNextMonthBtn.click();
		giftDropOffNextMonthBtn.click();
		giftDropOffStartDate.click();
		giftDropOffStartDateWidget.click();
		comLibs.scrollMid();
		
		contactPersonTextField.sendKeys(Property.getProperty("contactPerson"));
		phoneNumberTextField.sendKeys(Property.getProperty("phoneNum"));
		emailTextField.sendKeys(Property.getProperty("emailID"));
		drivePreviewBtn.click();
	}
	
	
	public boolean previewDriveWizard() {
		
		
		return driveName.isDisplayed();
		
		
	}
	
	public void postDriveBtnClick(){
		
		try {
			comLibs.scrollDown();
			postDriveBtn.click();
			Thread.sleep(10000);
			okBtn.click();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public boolean vDrivePostedStatus() {
		searchDriveTextField.sendKeys(Property.getProperty("driveName"));
		comLibs.waitForPageToLoad();
		comLibs.scrollDown();
		//
		//comLibs.WaitForElementToBeVisible(driveActiveStatusIcon);
		//comLibs.waitForXpathVisible("driveActiveStatusIcon");
		
		return driveActiveStatusIcon.isDisplayed();
	}
	
	public void drivepostedAlertDisp() {
		
		comLibs.aceptAlert();
	
	}
	
	}


