package com.agiliz.roonga.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.agiliz.roonga.util.Driver;



/**
 * Hello world!
 *
 */
public class LoginPage {
public WebDriver driver;

   @FindBy(linkText="Login")
   public WebElement loginLink;
   
   @FindBy(xpath="//input[@id='email']")
   public WebElement emailTextField;
   
   @FindBy(id="password")
   public WebElement pwdTextField;
   
   @FindBy(id="loginBtn")
   public WebElement loginButton;
   
   
   
   


public LoginPage(WebDriver driver) {
	// TODO Auto-generated constructor stub
	PageFactory.initElements(Driver.driver,this);
}












public void login(String uName,String pwd) throws Exception {
	   loginLink.click();
	   Thread.sleep(10000);
	emailTextField.sendKeys(uName);
	   Thread.sleep(5000);
	   pwdTextField.sendKeys(pwd);
	   Thread.sleep(5000);
	   loginButton.click();
	   Thread.sleep(5000);
}
}

