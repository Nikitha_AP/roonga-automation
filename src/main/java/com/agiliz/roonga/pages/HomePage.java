package com.agiliz.roonga.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class HomePage {

	
	public HomePage(WebDriver driver) {
		PageFactory.initElements(driver,this);
		// TODO Auto-generated constructor stub
	}


	public HomePage() {
		// TODO Auto-generated constructor stub
	}
	
	@FindBy(xpath="//a[text()='Home ']")
	public WebElement homeLink;
	
	
	@FindBy(xpath="//a[text()='About ']")
	public WebElement aboutLink;
	
	@FindBy(xpath="//*[@id='content1']/../button")
	public WebElement ourServicesLink;
	
	@FindBy(xpath="//*[@id='content2']/../button")
	public WebElement whoWeServe;
	
	@FindBy(xpath="//*[@id='myNavbar']/ul/li[5]/a")
	public WebElement ourDrivesLink;
	

	@FindBy(xpath="//*[@id='myNavbar']/ul/li[6]/a")
	public WebElement loginLink;
	
	@FindBy(xpath="//*[@id='myNavbar']/ul/li[7]/a/span")
	public WebElement contactBtn;
	
	@FindBy(xpath="//div[text()='Company']")
	public WebElement companyFooterLink;
	
	@FindBy(xpath="//a[text()='About']")
	public WebElement aboutFooterLink;
	
	@FindBy(xpath="//*[@class='containr']/div/div[1]/div[3]/a")
	public WebElement ourDrivesFooterLink;
	
	
	@FindBy(xpath="//*[@class='containr']/div/div[1]/div[4]/a")
	public WebElement contactFooterLink;
	
	@FindBy(xpath="//*[text()='Company']")
	public WebElement comapanyFooterText;
	

	@FindBy(xpath="//*[text()='Our Services']")
	public WebElement ourServicesFooterText;
	
	@FindBy(xpath="//*[text()='Collection Drives']")
	public WebElement collectionDriveLink;
	
	@FindBy(xpath="//*[text()='Adopt-a-Family']")
	public WebElement adoptionDriveLink;
	
	@FindBy(xpath="//*[text()='Direct Purchasing']")
	public WebElement directPurchasingLink;
	
	@FindBy(xpath="//*[text()='Who We Serve']")
	public WebElement whoWeServeText;
	
	@FindBy(xpath="//*[text()='Nonprofits']")
	public WebElement nonProfitLink;
	

	@FindBy(xpath="//*[text()='Companies']")
	public WebElement companiesLink;
	
	@FindBy(xpath="//*[text()='Religious Groups']")
	public WebElement religiousGroupsLink;
	
	//Home link display check
	public boolean homeButtonCheck(){
		boolean homeButton = homeLink.isDisplayed();
				Assert.assertEquals(homeButton, true);
				
		return homeButton;
		
	}
	
	//about display check
	public boolean aboutLinkCheck(){
		boolean aboutDisp = aboutLink.isDisplayed();
				Assert.assertEquals(aboutDisp, true);
				return aboutDisp;
				
	}
	
	//our services display check
	public boolean ourServicesCheck(){
		boolean ourServicesDisp = ourServicesLink.isDisplayed();
		Assert.assertEquals(ourServicesDisp, true);
		return ourServicesDisp;
		
	}
	
	
	//who we serve disp check
	public boolean whoWeServeCheck(){
		boolean whoWeServeDisp = whoWeServe.isDisplayed();
				Assert.assertEquals(whoWeServeDisp, true);
				return whoWeServeDisp;
		
	}
	
	//our drives disp check
	public boolean ourDrivesCheck(){
		boolean ourDrivesDisp = ourDrivesLink.isDisplayed();
		Assert.assertEquals(ourDrivesDisp, true);
		return ourDrivesDisp;
		
	}
	
	public boolean vLoginLinkDisplay() {
		boolean loginLinkDisp = loginLink.isDisplayed();
		Assert.assertEquals(loginLinkDisp, true);
		return loginLinkDisp;
		
	}
	
	public boolean vContactBtnDisp() {
		boolean contactBtnDisp = contactBtn.isDisplayed();
		Assert.assertEquals(contactBtnDisp, true);
		return contactBtnDisp;
		
	}
	
	public boolean vCompanyLinkDisp() {
		boolean companyLinkDisp = companyFooterLink.isDisplayed();
		Assert.assertEquals(companyLinkDisp, true);
		return companyLinkDisp;
	}
	
	public boolean vAboutFooterLink() {
		boolean aboutFooterLinkDisp = aboutFooterLink.isDisplayed();
		Assert.assertEquals(aboutFooterLinkDisp, true);
		return aboutFooterLinkDisp;
		
	}
	
	public boolean vOurDrivesFooterLink() {
		boolean ouDrivesFooterLinkDisp = ourDrivesFooterLink.isDisplayed();
		Assert.assertEquals(ouDrivesFooterLinkDisp, true);
		return ouDrivesFooterLinkDisp;
		
	}
	
	public boolean vContactFooterLink() {
		boolean contactFooterLinkDisp = contactFooterLink.isDisplayed();
		Assert.assertEquals(contactFooterLinkDisp, true);
		return contactFooterLinkDisp;
		
	}
	
	public boolean vCollectionDriveFooterLink() {
		boolean collectionDriveFooterLinkDisp = collectionDriveLink.isDisplayed();
		Assert.assertEquals(collectionDriveFooterLinkDisp, true);
		return collectionDriveFooterLinkDisp;
		
	}
	
	public boolean vAdoptionDriveFooterLink() {
		boolean adoptionDriveFooterLinkDisp = adoptionDriveLink.isDisplayed();
		Assert.assertEquals(adoptionDriveFooterLinkDisp, true);
		return adoptionDriveFooterLinkDisp;
		
	}
	
	public boolean vDirectPurchasingFooterLink() {
		boolean directPurchasingFooterLinkDisp = directPurchasingLink.isDisplayed();
		Assert.assertEquals(directPurchasingFooterLinkDisp, true);
		return directPurchasingFooterLinkDisp;
		
	}
	
	public boolean vNonProfitFooterLink() {
		boolean nonProfitFooterLinkDisp = nonProfitLink.isDisplayed();
		Assert.assertEquals(nonProfitFooterLinkDisp, true);
		return nonProfitFooterLinkDisp;
		
	}
	
	public boolean vCompaniesFooterLink() {
		boolean companyFooterLinkDisp = companyFooterLink.isDisplayed();
		Assert.assertEquals(companyFooterLinkDisp, true);
		return companyFooterLinkDisp;
		
	}
	
	public boolean vReligiousGroupsFooterLink() {
		boolean religiousGroupFooterLinkDisp = aboutFooterLink.isDisplayed();
		Assert.assertEquals(religiousGroupFooterLinkDisp, true);
		return religiousGroupFooterLinkDisp;
		
	}
}
